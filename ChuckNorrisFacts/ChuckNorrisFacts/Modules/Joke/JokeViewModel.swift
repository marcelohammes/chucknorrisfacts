//
//  JokeViewModel.swift
//  ChuckNorrisFacts
//
//  Created by Marcelo Hammes on 21/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Kingfisher
import RxKingfisher

struct JokeViewModel {

	private let category: Driver<String>

	private var jokeSubject = PublishSubject<Joke?>()

	var jokeDriver: Driver<Joke?> {
		return jokeSubject.asDriver(onErrorJustReturn: nil)
	}

	private let errorSubject = PublishSubject<String>()
	var onError: Driver<String> {
		return errorSubject.asDriver(onErrorRecover: { _ in .empty() })
	}

	private let provider: JokeDataProviderType
	private let disposeBag = DisposeBag()

	private var iconDataSubject = PublishSubject<Data>()
	var iconDataDriver: Driver<Data> {
		return iconDataSubject.asDriver(onErrorJustReturn: Data())
	}

	var title: Driver<String> {
		return category
	}

	var value: Driver<String?> {
		return jokeDriver.map { $0?.value }
	}

	var icon: Driver<URL?> {
		return jokeDriver.map { $0?.iconURL }
	}

	init(category: String, provider: JokeDataProviderType = JokeDataProvider()) {
		self.category = .just(category.capitalized)
		self.provider = provider

		loadJoke(category: category)
	}

	private func loadJoke(category: String) {
		provider
			.fetchJoke(category: category)
			.catchError { error in
				self.errorSubject.onNext(error.localizedDescription)
				return .empty()
		}
		.bind(to: jokeSubject)
		.disposed(by: disposeBag)
	}

}
