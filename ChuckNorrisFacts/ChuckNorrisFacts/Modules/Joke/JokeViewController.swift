//
//  JokeViewController.swift
//  ChuckNorrisFacts
//
//  Created by Marcelo Hammes on 21/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher
import RxKingfisher

final class JokeViewController: UIViewController {

	var viewModel: JokeViewModel?

	private let disposeBag = DisposeBag()

	@IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet private weak var iconImageView: UIImageView!
	@IBOutlet private weak var jokeLabel: UILabel!

	override func viewDidLoad() {
		super.viewDidLoad()

		self.view.accessibilityIdentifier = "jokeView"
		self.jokeLabel.accessibilityIdentifier = "jokeValueLabel"

		setup()
	}

	private func setup() {
		setupError()
		setupTitle()
		setupContent()
		setupActivityIndicator()
	}

	private func setupError() {
		viewModel?.onError
			.drive(onNext: { errorMessage in
				let alertController = UIAlertController(title: NSLocalizedString("Error", comment: ""),
																								message: errorMessage,
																								preferredStyle: .alert)
				let buttonOK = UIAlertAction(title: NSLocalizedString("OK", comment: ""),
																		 style: .cancel,
																		 handler: { _ in
																			self.navigationController?.popViewController(animated: true)
				})
				alertController.addAction(buttonOK)
				self.present(alertController, animated: true)
			})
			.disposed(by: disposeBag)
	}

	private func setupTitle() {
		viewModel?.title
			.drive(navigationItem.rx.title)
			.disposed(by: disposeBag)
	}

	private func setupContent() {
		viewModel?.value
			.drive(jokeLabel.rx.text)
			.disposed(by: disposeBag)

		viewModel?.icon
			.drive(iconImageView.kf.rx.image())
			.disposed(by: disposeBag)

		viewModel?.value
			.map { _ in false }
			.startWith(false)
			.drive(jokeLabel.rx.isHidden)
			.disposed(by: disposeBag)

		viewModel?.icon
			.map { _ in false }
			.startWith(false)
			.drive(iconImageView.rx.isHidden)
			.disposed(by: disposeBag)
	}

	private func setupActivityIndicator() {
		viewModel?.jokeDriver
			.map { _ in false }
			.startWith(true)
			.drive(activityIndicator.rx.isAnimating)
			.disposed(by: disposeBag)

		viewModel?.jokeDriver
			.map { _ in false }
			.startWith(true)
			.map { !$0 }
			.drive(activityIndicator.rx.isHidden)
			.disposed(by: disposeBag)
	}

}
