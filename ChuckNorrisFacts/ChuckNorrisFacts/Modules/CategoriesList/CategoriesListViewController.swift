//
//  CategoriesListViewController.swift
//  ChuckNorrisFacts
//
//  Created by Marcelo Hammes on 21/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class CategoriesListViewController: UIViewController {

	private static let cellIdentifier = "CategoryCell"

	@IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet private weak var categoriesTableView: UITableView!

	private var viewModel: CategoriesListViewModel?
	private let disposeBag = DisposeBag()

	override func viewDidLoad() {
		super.viewDidLoad()

		self.title = NSLocalizedString("Categories", comment: "")
		self.view.accessibilityIdentifier = "categoriesListView"

		setup()
		setupTableViewObservables()
	}

	private func setup() {
		let viewModel = CategoriesListViewModel()
		self.viewModel = viewModel

		setupError()
		setupActivityIndicator()
	}

	private func setupActivityIndicator() {
		viewModel?.categoriesDriver
			.map { _ in false }
			.startWith(true)
			.drive(activityIndicator.rx.isAnimating)
			.disposed(by: disposeBag)

		viewModel?.categoriesDriver
			.map { _ in false }
			.startWith(true)
			.map { !$0 }
			.drive(activityIndicator.rx.isHidden)
			.disposed(by: disposeBag)
	}

	private func setupError() {
		viewModel?.onError
			.drive(onNext: { errorMessage in
				let alertController = UIAlertController(title: NSLocalizedString("Error", comment: ""),
																								message: errorMessage,
																								preferredStyle: .alert)
				let buttonOK = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .cancel)
				alertController.addAction(buttonOK)
				self.present(alertController, animated: true)
			})
			.disposed(by: disposeBag)
	}

	private func setupTableViewObservables() {
		guard let viewModel = viewModel else { return }
		viewModel.categoriesDriver
			.map { _ in false }
			.startWith(true)
			.map { $0 }
			.drive(categoriesTableView.rx.isHidden)
			.disposed(by: disposeBag)

		disposeBag.insert(
			viewModel.categoriesDriver
				.drive(categoriesTableView.rx.items(cellIdentifier: CategoriesListViewController.cellIdentifier,
																						cellType: CategoryCell.self)) { _, category, cell in
																							cell.setup(CategoryCellViewModel(category: category))
			},

			categoriesTableView.rx.modelSelected(String.self)
				.subscribe(onNext: { [weak self] category in
					self?.openJokeScreen(category: category)
				})
		)
	}

	private func openJokeScreen(category: String) {
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		guard let jokeViewController =
			storyboard.instantiateViewController(withIdentifier: "Joke") as? JokeViewController else {
				return
		}

		jokeViewController.viewModel = JokeViewModel(category: category)

		navigationController?.pushViewController(jokeViewController, animated: true)
	}
}
