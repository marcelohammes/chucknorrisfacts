//
//  CategoriesListViewModel.swift
//  ChuckNorrisFacts
//
//  Created by Marcelo Hammes on 21/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct CategoriesListViewModel {

	private let errorSubject = PublishSubject<String>()
	private let provider: CategoriesDataProviderType
	private let disposeBag = DisposeBag()

	private var categoriesSubject = PublishSubject<[String]>()

	var categoriesDriver: Driver<[String]> {
		return categoriesSubject.asDriver(onErrorJustReturn: [])
	}

	var onError: Driver<String> {
		return errorSubject.asDriver(onErrorRecover: { _ in .empty() })
	}

	init(provider: CategoriesDataProviderType = CategoriesDataProvider()) {
		self.provider = provider

		loadCategories()
	}

	private func loadCategories() {
		provider
			.fetchCategories()
			.catchError { error in
				self.errorSubject.onNext(error.localizedDescription)
				return .empty()
		}
		.bind(to: categoriesSubject)
		.disposed(by: disposeBag)
	}

}
