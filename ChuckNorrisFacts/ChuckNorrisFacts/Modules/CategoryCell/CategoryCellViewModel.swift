//
//  CategoryCellViewModel.swift
//  ChuckNorrisFacts
//
//  Created by Marcelo Hammes on 21/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

struct CategoryCellViewModel {
	private let category: String

	var title: String {
		return category.capitalized
	}

	init(category: String) {
		self.category = category
	}
}
