//
//  CategoryCell.swift
//  ChuckNorrisFacts
//
//  Created by Marcelo Hammes on 21/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import UIKit
import RxSwift

final class CategoryCell: UITableViewCell {

	@IBOutlet private weak var titleLabel: UILabel!

	private var viewModel: CategoryCellViewModel?

	override func prepareForReuse() {
		super.prepareForReuse()

		titleLabel?.text = nil
	}

	func setup(_ viewModel: CategoryCellViewModel) {
		self.viewModel = viewModel

		titleLabel?.text = viewModel.title
	}
}
