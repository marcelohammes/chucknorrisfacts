//
//  CategoriesAPIResponse.swift
//  ChuckNorrisFacts
//
//  Created by Marcelo Hammes on 21/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import Foundation

struct CategoriesAPIResponse: Decodable {
	let categories: [String]

	init(from decoder: Decoder) throws {
		let container = try decoder.singleValueContainer()

		categories = try container.decode([String].self)
	}
}
