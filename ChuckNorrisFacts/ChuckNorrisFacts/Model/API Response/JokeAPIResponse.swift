//
//  JokeAPIResponse.swift
//  ChuckNorrisFacts
//
//  Created by Marcelo Hammes on 20/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import Foundation

struct JokeAPIResponse: Decodable {
	let joke: Joke

	init(from decoder: Decoder) throws {
		let container = try decoder.singleValueContainer()

		joke = try container.decode(Joke.self)
	}
}
