//
//  Joke.swift
//  ChuckNorrisFacts
//
//  Created by Marcelo Hammes on 20/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import Foundation

struct Joke: Decodable {
	let id: String
	let createdAt: Date
	let iconURL: URL
	let updatedAt: Date
	let url: URL
	let value: String
	let categories: [String]

	enum CodingKeys: String, CodingKey {
		case id
		case createdAt = "created_at"
		case iconURL = "icon_url"
		case updatedAt = "updated_at"
		case url
		case value
		case categories
	}
}
