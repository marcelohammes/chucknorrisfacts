//
//  CategoriesDataProvider.swift
//  ChuckNorrisFacts
//
//  Created by Marcelo Hammes on 21/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import RxSwift
import Moya

protocol CategoriesDataProviderType {
	func fetchCategories() -> Observable<[String]>
}

protocol CategoriesDataTestProviderType {
	mutating func fetchCategories(stub: MoyaProvider<JokesEndpoint>) -> Observable<[String]>
}

struct CategoriesDataProvider: CategoriesDataProviderType {

	private var provider = MoyaProvider<JokesEndpoint>()

	func fetchCategories() -> Observable<[String]> {
		return provider.rx.request(.categories)
			.map(CategoriesAPIResponse.self)
			.map { $0.categories }
			.asObservable()
	}
}

extension CategoriesDataProvider: CategoriesDataTestProviderType {
	mutating func fetchCategories(stub: MoyaProvider<JokesEndpoint>) -> Observable<[String]> {
		provider = stub
		return fetchCategories()
	}
}
