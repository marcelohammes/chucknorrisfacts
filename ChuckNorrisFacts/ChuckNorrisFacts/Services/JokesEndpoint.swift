//
//  JokesEndpoint.swift
//  ChuckNorrisFacts
//
//  Created by Marcelo Hammes on 20/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import Foundation
import Moya

enum JokesEndpoint {
	case jokes(category: String)
	case categories
}

extension JokesEndpoint: TargetType {
	var baseURL: URL {
		return URL(string: "https://api.chucknorris.io/")!
	}

	var path: String {
		switch self {
		case .jokes:
			return "/jokes/random"
		case .categories:
			return "jokes/categories"
		}
	}

	var method: Moya.Method {
		return .get
	}

	var sampleData: Data {
		switch self {
		case .jokes:
			return "{\"categories\":[\"science\"],\"created_at\":\"2020-01-05 13:42:19.324003\",\"icon_url\":\"https://assets.chucknorris.host/img/avatar/chuck-norris.png\",\"id\":\"u-xwyw6rq0-dxrq1rioogg\",\"updated_at\":\"2020-01-05 13:42:19.324003\",\"url\":\"https://api.chucknorris.io/jokes/u-xwyw6rq0-dxrq1rioogg\",\"value\":\"Chuck Norris once roundhouse kicked someone so hard that his foot broke the speed of light, went back in time, and killed Amelia Earhart while she was flying over the Pacific Ocean.\"}".data(using: .utf8)!
		case .categories:
			return "[\"animal\",\"career\",\"celebrity\",\"dev\",\"explicit\",\"fashion\",\"food\",\"history\",\"money\",\"movie\",\"music\",\"political\",\"religion\",\"science\",\"sport\",\"travel\"]".data(using: .utf8)!
		}
	}

	var task: Task {
		var parameters: [String: Any] = [:]

		switch self {
		case .jokes(let category):
			parameters["category"] = category
		case .categories:
			break
		}

		return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
	}

	var headers: [String : String]? {
		return ["Content-Type": "application/json"]
	}
}
