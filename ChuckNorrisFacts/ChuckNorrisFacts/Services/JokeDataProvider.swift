//
//  JokeDataProvider.swift
//  ChuckNorrisFacts
//
//  Created by Marcelo Hammes on 20/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import RxSwift
import Moya

protocol JokeDataProviderType {
	func fetchJoke(category: String) -> Observable<Joke>
}

protocol JokeDataTestProviderType {
	mutating func fetchJoke(category: String, stub: MoyaProvider<JokesEndpoint>) -> Observable<Joke>
}

struct JokeDataProvider: JokeDataProviderType {

	private var provider = MoyaProvider<JokesEndpoint>()

	func fetchJoke(category: String) -> Observable<Joke> {
		let decoder = JSONDecoder()

		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"

		decoder.dateDecodingStrategy = .formatted(dateFormatter)

		return provider.rx.request(.jokes(category: category))
			.map(JokeAPIResponse.self, using: decoder)
			.map { $0.joke }
			.asObservable()
	}
}

extension JokeDataProvider: JokeDataTestProviderType {
	mutating func fetchJoke(category: String, stub: MoyaProvider<JokesEndpoint>) -> Observable<Joke> {
		provider = stub
		return fetchJoke(category: category)
	}

}
