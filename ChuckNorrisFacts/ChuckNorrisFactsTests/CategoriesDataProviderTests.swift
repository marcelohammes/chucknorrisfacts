//
//  CategoriesDataProviderTests.swift
//  ChuckNorrisFactsTests
//
//  Created by Marcelo Hammes on 22/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import XCTest
import Moya
import RxSwift

@testable import ChuckNorrisFacts

class CategoriesDataProviderTests: XCTestCase {

	var categoriesProvider: CategoriesDataTestProviderType = CategoriesDataProvider()
	let stubbingProvider = MoyaProvider<JokesEndpoint>(stubClosure: MoyaProvider.immediatelyStub)
	let disposeBag = DisposeBag()

	override func setUp() { }

	override func tearDown() { }

	func testCategoriesList() {
		let expectation = self.expectation(description: "request categories list")

		categoriesProvider.fetchCategories(stub: stubbingProvider)
			.subscribe(onNext: { categories in
				XCTAssertGreaterThan(categories.count, 0, "empty categories array")

				expectation.fulfill()
			})
			.disposed(by: disposeBag)
		self.waitForExpectations(timeout: 5.0, handler: nil)
	}
}
