//
//  JokeDataProviderTests.swift
//  ChuckNorrisFactsTests
//
//  Created by Marcelo Hammes on 20/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import XCTest
import Moya
import RxSwift

@testable import ChuckNorrisFacts

class JokeDataProviderTests: XCTestCase {

	var jokeProvider: JokeDataTestProviderType = JokeDataProvider()
	let stubbingProvider = MoyaProvider<JokesEndpoint>(stubClosure: MoyaProvider.immediatelyStub)
	let disposeBag = DisposeBag()

	override func setUp() {}

	override func tearDown() {}

	func testRandomJoke() {
		let expectation = self.expectation(description: "request a random joke")

		jokeProvider.fetchJoke(category: "science", stub: stubbingProvider)
			.subscribe(onNext: { joke in
				XCTAssertNotNil(joke)
				XCTAssertNotNil(joke.iconURL)
				XCTAssertNotNil(joke.value)

				expectation.fulfill()
			})
			.disposed(by: disposeBag)
		self.waitForExpectations(timeout: 5.0, handler: nil)
	}
}
