//
//  ChuckNorrisFactsUITests.swift
//  ChuckNorrisFactsUITests
//
//  Created by Marcelo Hammes on 22/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import XCTest

class ChuckNorrisFactsUITests: XCTestCase {

	var app: XCUIApplication!

	override func setUp() {
		continueAfterFailure = false

		app = XCUIApplication()
	}

	override func tearDown() { }

	func testOpeningCategories() {
		app.launch()

		XCTAssertTrue(app.isDisplayingCategoriesList)
	}

	func testTapCategoryToShowJoke() {
		app.launch()

		XCTAssertTrue(app.isDisplayingCategoriesList)

		guard app.tables.element(boundBy: 0).waitForExistence(timeout: 10.0) else {
			XCTFail("timeout while load categories")
			return
		}

		guard app.tables.element(boundBy: 0).cells.count > 0 else {
			XCTFail("empty table")
			return
		}

		app.tables.element(boundBy: 0).cells.element(boundBy: 0).tap()

		XCTAssertTrue(app.isDisplayingJokeView)

		guard app.staticTexts["jokeValueLabel"].waitForExistence(timeout: 20.0) else {
			XCTFail("timeout while load joke")
			return
		}

		XCTAssertNotNil(app.staticTexts["jokeValueLabel"].label)
		XCTAssertNotEqual(app.staticTexts["jokeValueLabel"].label, "")
	}
}
