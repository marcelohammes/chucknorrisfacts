//
//  XCUIApplication.swift
//  ChuckNorrisFactsUITests
//
//  Created by Marcelo Hammes on 22/01/20.
//  Copyright © 2020 Marcelo Hammes. All rights reserved.
//

import XCTest

extension XCUIApplication {
	var isDisplayingCategoriesList: Bool {
		return otherElements["categoriesListView"].exists
	}

	var isDisplayingJokeView: Bool {
		return otherElements["jokeView"].exists
	}

	var isDisplayingJoke: Bool {
		return otherElements["jokeIconImageView"].exists && otherElements["jokeValueLabel"].exists
	}
}
